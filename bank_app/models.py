from __future__ import annotations
from decimal import Decimal
from django.conf import settings
from django.db import models, transaction
from django.db.models import Q
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
import pkg_resources
from .errors import InsufficientFunds
import os
import random


BANK_NAME=os.environ['BANK_NAME']

class AccountUID(models.Model):
    @classmethod
    def uid(cls):
        id = models.UUIDField(primary_key=True, editable=False)
        ids = cls.objects.all()
        genid = random.randint(100, 5000000)
        while True:
            if genid in ids:
                genid = random.randint(100, 5000000)
            else:
                return cls.objects.create(id=genid)
    def __str__(self):
        return f'{self.pk}'

class BankUID(models.Model):
    @classmethod
    def uid(cls):
        id = models.UUIDField(primary_key=True, editable=False)
        ids = cls.objects.all()
        genid = random.randint(100, 5000000)
        while True:
            if genid in ids:
                genid = random.randint(100, 5000000)
            else:
                return cls.objects.create(id=genid)
    def __str__(self):
        return f'{self.pk}'


class Rank(models.Model):
    name        = models.CharField(max_length=35, unique=True, db_index=True)
    value       = models.IntegerField(unique=True, db_index=True)

    @classmethod
    def default_rank(cls) -> Rank:
        return cls.object.all().aggregate(models.Min('value'))['value__min']

    def __str__(self):
        return f'{self.value}:{self.name}'

class Customer(models.Model):
    user        = models.OneToOneField(User, primary_key=True, on_delete=models.PROTECT)
    rank        = models.ForeignKey(Rank, default=2, on_delete=models.PROTECT)
    personal_id = models.IntegerField(db_index=True, default=2)
    phone       = models.CharField(max_length=35, db_index=True)

    @property
    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    @property
    def default_account(self) -> Account:
        return Account.objects.filter(user=self.user).first()


    @property
    def can_make_loan(self) -> bool:
        return self.rank.value >= settings.CUSTOMER_RANK_LOAN


    def make_loan(self, amount, name):
        assert self.can_make_loan, 'User rank does not allow for making loans.'
        assert amount >= 0, 'Negative amount not allowed for loan.'
        loan_account = AccountUID.uid()
        loan = Account.objects.create(user=self.user, account_number=loan_account, name=f'Loan: {name}')
        bank = Bank.objects.get(bank_name=BANK_NAME)
        Ledger.transfer(
            amount,
            loan,
            bank,
            f'Loan paid out to account {self.default_account}',
            self.default_account,
            bank,
            f'Credit from loan {loan.pk}: {loan.name}',
            is_loan=True
        )

    @classmethod
    def search(cls, search_term):
        return cls.objects.filter(
            Q(user__username__contains=search_term)     |
            Q(user__first_name__contains=search_term)   |
            Q(user__last_name__contains=search_term)    |
            Q(user__email__contains=search_term)        |
            Q(personal_id__contains=search_term)        |
            Q(phone__contains=search_term)
        )[:15]

    def __str__(self):
        return f'{self.personal_id}: {self.full_name}'

class Bank(models.Model):
    bank_number = models.OneToOneField(BankUID, default=BankUID.uid, primary_key=True, editable=False, on_delete=models.PROTECT)
    bank_name = models.TextField()
    bank_ip_address = models.TextField()
    def __str__(self):
        return f'{self.bank_number} :: {self.bank_name} :: {self.bank_ip_address}'


class Account(models.Model):
    account_number = models.OneToOneField(AccountUID, default=AccountUID.uid, primary_key=True, editable=False, on_delete=models.PROTECT)
    user        = models.ForeignKey(User, on_delete=models.PROTECT)
    name        = models.CharField(max_length=50, db_index=True)

    class Meta:
        get_latest_by ='pk'

    @property
    def movements(self) -> QuerySet:
        return Ledger.objects.filter(account=self)

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    def __str__(self):
        return f'{self.pk} :: {self.user} :: {self.name}'

class Card_type(models.Model):
    name       = models.CharField(max_length=35, unique=True, db_index=True)
    prefix     = models.IntegerField(default=0000, db_index=True)

    def __str__(self):
         return f'{self.name} :: {self.prefix} :: {self.pk}'


def create_unique_card_number():
    N = 12
    is_not_unique = True
    while is_not_unique:
        minimum = pow(10, N-1)
        maximum = pow(10, N) - 1
        cardnumber = random.randint(minimum, maximum)
        if not Card.objects.filter(cardnumber=cardnumber):
            is_not_unique = False
    return cardnumber

def create_random_cvc():
    N = 3
    minimum = pow(10, N-1)
    maximum = pow(10, N) - 1
    randomnumber = str(random.randint(minimum, maximum))
    return randomnumber

class Card(models.Model):
    user        = models.ForeignKey(User, on_delete=models.PROTECT)
    account     = models.ForeignKey(Account, on_delete=models.PROTECT)
    card_type     = models.ForeignKey(Card_type, default=Card_type, on_delete=models.PROTECT)
    cvc        = models.CharField(max_length=3, null=True, db_index=True, default=create_random_cvc)
    cardnumber  = models.CharField(max_length=12, null=True, unique=True, db_index=True, default=create_unique_card_number)
    expiration_date = models.DateField(auto_now_add=False, db_index=True)
    
    @property
    def cards(self) -> QuerySet:
        return Card.objects.filter(user=self.user)

    def __str__(self):
        return f'{self.pk} :: {self.user} :: {self.account.name} :: {self.card_type.name}  :: {self.card_type.prefix}{self.cardnumber}'


class Card_request(models.Model):
    reciever = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reciever")
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="sender", blank=True)
    card_type = models.ForeignKey(Card_type, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    is_accepted = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)

    @property
    def requests(self) -> QuerySet:
        return Card.objects.filter(user=self.user)
   
    def __str__(self):
        return f' {self.pk} :: {self.sender} :: {self.is_active} :: {self.card_type.name}'

class UID(models.Model):
    @classmethod
    @property
    def uid(cls):
        return cls.objects.create()

    def __str__(self):
        return f'{self.pk}'

class Ledger(models.Model):
    account     = models.ForeignKey(Account, on_delete=models.PROTECT)
    transaction = models.ForeignKey(UID, on_delete=models.PROTECT)
    amount      = models.DecimalField(max_digits=10, decimal_places=2)
    bank_number = models.ForeignKey(Bank, on_delete=models.PROTECT)
    timestamp   = models.DateTimeField(auto_now_add=True, db_index=True)
    text        = models.TextField()

    @classmethod
    def transfer(cls, amount, debit_account, debit_text, bank_number_debit, credit_account, bank_number_credit, credit_text, is_loan=False) -> int:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                uid = UID.uid
                cls(amount=-amount, transaction=uid, account=debit_account, bank_number=bank_number_debit, text=debit_text).save()
                cls(amount=amount, transaction=uid, account=credit_account, bank_number=bank_number_credit, text=credit_text).save()
            else:
                raise InsufficientFunds
        return uid

    def __str__(self):
        return f'{self.amount} :: {self.transaction} :: {self.timestamp} :: {self.account} :: {self.text}'



class ExternalTransferData(models.Model):
    transaction = models.ForeignKey(Ledger, on_delete=models.PROTECT)
    account = models.TextField()
    debit_account = models.TextField()
    external_bank_number = models.TextField()
    external_transaction_id = models.TextField()
    credit_text = models.TextField()
    def __str__(self):
        return f'{self.transaction} :: {self.account} :: {self.debit_account} :: {self.credit_text}'