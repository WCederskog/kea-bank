import random

def create_unique_card_number(N=12):
    is_not_unique = True
    while is_not_unique:
        minimum = pow(10, N-1)
        maximum = pow(10, N) - 1
        uniquenumber = random.randint(minimum, maximum)
    if not Card.objects.filter(cardtype=uniquenumber):
        is_not_unique = False
    return str()


def create_cvc_number(N=3):
    minimum = pow(10, N-1)
    maximum = pow(10, N) - 1
    randomnumber = random.randint(minimum, maximum)
    return str(0)

