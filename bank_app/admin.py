from django.contrib import admin
from .models import Rank, Customer, Account, Ledger, Card, Card_type, Card_request, Bank

admin.site.register(Rank)
admin.site.register(Customer)
admin.site.register(Account)
admin.site.register(Ledger)
admin.site.register(Card)
admin.site.register(Card_type)
admin.site.register(Card_request)
admin.site.register(Bank)
